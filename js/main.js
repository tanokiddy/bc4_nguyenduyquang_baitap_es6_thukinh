import { dataGlasses } from "./model.js";
import { addInfo, createEl } from "./controller.js";

createEl();
let glassesHistory = [];
var lastIndexHistory;
dataGlasses.forEach(function (glassessItem, index) {
  let div = document.createElement("div");
  div.className = "col-4 vglasses__items";
  div.setAttribute("id", "glasses");
  div.style.backgroundImage = "url(" + glassessItem.src + ")";
  div.onclick = function () {
    if (dataGlasses[index].id == dataGlasses.id) {
      return index;
    }
    addInfo(index);
    glassesHistory.push(dataGlasses[index]);
    return (lastIndexHistory = glassesHistory.length - 1);
  };
  document.querySelector("#vglassesList").appendChild(div);
});
//============SETTING BEFORE BUTTON=================
document.querySelector("#btnBefore").onclick = function () {
  if (lastIndexHistory > 0) {
    lastIndexHistory--;
    console.log("glassesHistory: ", glassesHistory);
    console.log("lastIndexHistory: ", lastIndexHistory);
    let i = lastIndexHistory;
    document.querySelector(".vglasses__info").style.display = "inline-block";
    document.querySelector(".addGlasses").style.opacity = "0.9";
    document.querySelector(".h1Title").innerText =
      glassesHistory[i].name +
      " - " +
      glassesHistory[i].brand +
      " " +
      glassesHistory[i].color;

    document.querySelector(".pPrice").innerText = "$" + glassesHistory[i].price;

    document.querySelector(".pInfo").innerText = glassesHistory[i].description;

    document.querySelector(".addGlasses").src = glassesHistory[i].virtualImg;
    return lastIndexHistory;
  } else {
    document.querySelector(".vglasses__info").style.display = "none";
    document.querySelector(".addGlasses").style.opacity = "0";
    return (lastIndexHistory = -1);
  }
};

//===========SETTING AFTER BUTTON=============
document.querySelector("#btnAfter").onclick = function () {
  if (lastIndexHistory < glassesHistory.length - 1) {
    lastIndexHistory++;
    var i = lastIndexHistory;
    document.querySelector(".vglasses__info").style.display = "inline-block";
    document.querySelector(".addGlasses").style.opacity = "0.9";
    document.querySelector(".h1Title").innerText =
      glassesHistory[i].name +
      " - " +
      glassesHistory[i].brand +
      " " +
      glassesHistory[i].color;

    document.querySelector(".pPrice").innerText = "$" + glassesHistory[i].price;

    document.querySelector(".pInfo").innerText = glassesHistory[i].description;

    document.querySelector(".addGlasses").src = glassesHistory[i].virtualImg;
    console.log("lastIndexHistory: ", lastIndexHistory);
    return lastIndexHistory;
  } else {
    return (lastIndexHistory = glassesHistory.length - 1);
  }
};
