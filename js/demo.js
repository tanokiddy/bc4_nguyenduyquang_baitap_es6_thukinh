import { dataGlasses } from "./model.js";
import { addInfo } from "./controller.js";
document.querySelector("#btnClick").onclick = function () {
  dataGlasses.forEach(function (glassessItem, index) {
    let div = document.createElement("div");
    div.className = "col-4 vglasses__items";
    div.onclick = function () {
      if (dataGlasses[index].id == dataGlasses.id) {
        return index;
      }
      addInfo(index);
    };
    div.style.backgroundImage = "url(" + glassessItem.src + ")";
    document.querySelector("#vglassesList").appendChild(div);
  });
};
// document.querySelector("#btnClick").onclick = function () {
//   var text = ["g1", "g2", "g3", "g4", "g5", "g6", "g7", "g8", "g9"];
//   text.forEach(function (i = 1) {
//     var div = document.createElement("div");
//     var link = "url(./img/" + i + ".jpg)";
//     i++;
//     div.className = "col-4 vglasses__items";
//     div.style.backgroundImage = link;
//     document.querySelector("#vglassesList").appendChild(div);
//   });
// };
