import { dataGlasses } from "./model.js";

export function addInfo(index) {
  document.querySelector(".vglasses__info").style.display = "inline-block";
  document.querySelector(".addGlasses").style.opacity = "0.9";
  document.querySelector(".h1Title").innerText =
    dataGlasses[index].name +
    " - " +
    dataGlasses[index].brand +
    " " +
    dataGlasses[index].color;

  document.querySelector(".pPrice").innerText = "$" + dataGlasses[index].price;

  document.querySelector(".pInfo").innerText = dataGlasses[index].description;

  document.querySelector(".addGlasses").src = dataGlasses[index].virtualImg;
}

export function createEl() {
  //create h5 element
  let h1Title = document.createElement("h5");
  h1Title.className = "h1Title";
  document.querySelector("#glassesInfo").appendChild(h1Title);
  //create span element
  let pPrice = document.createElement("span");
  pPrice.className = "pPrice";
  document.querySelector("#glassesInfo").appendChild(pPrice);
  //create p element
  let pInfo = document.createElement("p");
  pInfo.className = "pInfo mt-3";
  document.querySelector("#glassesInfo").appendChild(pInfo);
  //create img element
  let addGlassess = document.createElement("img");
  addGlassess.className = "addGlasses";
  document.querySelector(".vglasses__model").appendChild(addGlassess);
}
